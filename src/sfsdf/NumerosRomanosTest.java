package sfsdf;

import conversor.Conversor;
import org.junit.test.Assert.*;

public class NumerosRomanosTest {
	private Conversor conversor;

@before
	public void setUp() throws Exception {
	conversor = new Conversor();
	
}

@test
public void testRomanosParaArabicos() {
	AssertEquals(1, conversor.RomanosParaArabicos("I"));
	AssertEquals(2, conversor.RomanosParaArabicos("II"));
	AssertEquals(2, conversor.RomanosParaArabicos("X"));
	
}
}
